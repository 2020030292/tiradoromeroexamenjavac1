package com.example.examencorte1java;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RectanguloActivity extends AppCompatActivity {

    private Rectangulo rectangulo;
    private TextView lblBase;
    private EditText txtBase;
    private TextView lblAltura;
    private EditText txtAltura;
    private TextView lblCalculoArea;
    private TextView lblArea;
    private TextView lblCalculoPerimetro;
    private TextView lblPerimetro;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo);
        Bundle datos = getIntent().getExtras();
        String usuario = datos.getString("usuario");
        lblUsuario.setText(usuario);


        lblBase = findViewById(R.id.lblBase);
        txtBase = findViewById(R.id.txtBase);
        lblAltura = findViewById(R.id.lblAltura);
        txtAltura = findViewById(R.id.txtAltura);
        lblCalculoArea = findViewById(R.id.lblCalculoArea);
        lblArea = findViewById(R.id.lblArea);
        lblCalculoPerimetro = findViewById(R.id.lblCalculoPerimetro);
        lblPerimetro = findViewById(R.id.lblPerimetro);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(txtBase.getText().toString()) || TextUtils.isEmpty(txtAltura.getText().toString())) {
                    Toast.makeText(RectanguloActivity.this, "Por favor, ingrese valores para la base y la altura", Toast.LENGTH_SHORT).show();
                } else {
                    rectangulo = new Rectangulo(Integer.parseInt(txtBase.getText().toString()), Integer.parseInt(txtAltura.getText().toString()));
                    lblArea.setText(String.valueOf(rectangulo.calcularArea()));
                    lblPerimetro.setText(String.valueOf(rectangulo.calcularPerimetro()));
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtBase.setText("");
                txtAltura.setText("");
                lblArea.setText("");
                lblPerimetro.setText("");
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}