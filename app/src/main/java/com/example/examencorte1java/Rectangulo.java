package com.example.examencorte1java;

public class Rectangulo {
    private int base;
    private int altura;

    public Rectangulo(int base, int altura) {
        this.base = base;
        this.altura = altura;

    }
    public float calcularArea() {
        return this.base * this.altura;
    }

    public float calcularPerimetro() {
        return 2 * (this.base + this.altura);
    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }
}



